### Java Streams

[Java Stream API / Javadoc](https://docs.oracle.com/javase/8/docs/api/index.html?java/util/stream/Stream.html)

[Original Artikel bei Medium](https://betterprogramming.pub/can-streams-replace-loops-in-java-f56d4461743a)

Achtung ```.toList()``` gibt ```unmodifiable``` Resultate

#### Strem Methoden

```java
allMatch anyMatch count filter findFirst forEach map reduce sorted limit
```

#### Filter

```java
// Schleife
List<String> itemNames = new ArrayList<>();
for (Item item : items) {
    if (item.type() == type) {
        itemNames.add(item.name());
    }
}

// Stream
List<String> itemNames = items.stream()
        .filter(item -> item.type() == type)
        .map(item -> item.name())
        .toList();
```

#### Zufallsliste

```java
public record Item(Type type, String name) {
    public enum Type {
        WEAPON, ARMOR, HELMET, GLOVES, BOOTS,
    }

    private static final Random random = new Random();
    private static final String[] NAMES = {
            "beginner",
            "knight",
            "king",
            "dragon",
    };

    public static Item random() {
        return new Item(
                Type.values()[random.nextInt(Type.values().length)],
                NAMES[random.nextInt(NAMES.length)]);
    }
}

// Schleife
List<Item> items = new ArrayList<>(100);
for (int i = 0; i < 100; i++) {
    items.add(Item.random());
}

// Stream
List<Item> items = Stream.generate(Item::random).limit(length).toList();
```

#### Daten Modifikation

```java
// Schleife
Map<Item.Type, Integer> map = new HashMap<>();
for (Item item : items) {
    map.compute(item.type(), (key, value) -> {
        if (value == null) return 1;
        return value + 1;
    });
}

// Stream
Map<Item.Type, Integer> map = items.stream().collect(Collectors.toMap(
    Item::type,
    value -> 1,
    Integer::sum));
    
// Das gleiche als parallel Stream
Map<Item.Type, Integer> map = items.parallelStream().collect(Collectors.toMap(
    Item::type,
    value -> 1,
    Integer::sum));
```

#### Steams mit Iteration Bedingungen

```java
// Stream
Stream.iterate(true, condition -> condition, condition -> doSomething())
    .forEach(unused -> ...);

// Schleife deutlich bessere Lesbarkeit
boolean condition = true;
while (condition) {
    condition = doSomething();
}

// Stream
IntStream.range(0, 10).forEach(i -> computation());

// Schleife
for (int i = 0; i < 10; i++) {
    computation();
}
```

#### Iterable zu Stream

```java
import java.nio.file.Path;
import static java.util.stream.StreamSupport.stream;

// Schleife 
for (Path part : path) {
    System.out.println(part);
}

// Stream
boolean parallel = false;
stream(path.spliterator(), parallel).forEach(part -> System.out.println(part));
```
