## MariaDB lokale Installation unter Windows ohne Admin Rechte

**!!! Achtung, es handelt um eine Anleitung zum Aufsetzen von MySQL Server zum Entwicklungszwecken !!!**

**!!! Es ist keine Anleitung zum Aufsetzen von MySQL Server in Produktion Umgebung !!!**

Installation Vorbereitungen

- Datenbank Installation Verzeichnis erstellen
- Überlegen, wo wird Datenbank Query Log Datei abgelegt
- Datenbank Daten Verzeichnis erstellen
- Anwendung Paket in **"zip"** Format herunterladen

Normalerweise wird Anwendung für Windows in **"msi"** Format bereitgestellt. Bei den jeweiligen Seiten 
gibt es aber auch Pakete in **"zip"** Format. Für die lokale Installation ohne Admin Rechte wird
Paket in **"zip"** benötigt.


In diese Anleitung wird folgende Beispielkonfiguration verwendet:

- Installation Verzeichnis: **"D:\Apps\MariaDb"**
- Datenbank Daten Verzeichnis: **"D:\Apps\MariaDb\data"**
- Datenbank Log Datei ist in: **"D:\Apps\MariaDb\query.log"**
- Eigenes Root Passwort: **"jM10L34GeF"**

##### 1. Datenbank initialisieren und Server Konfiguration Datei erstellen

Anwendung in Installationsverzeichnis entpacken. Datenbank **"bin"** Verzeichnis muss, sich nun direkt in Installationsverzeichnis befinden

```bat
cd D:\Apps\MariaDb\bin
mariadb-install-db.exe --datadir=D:/Apps/MariaDb/data --password=jM10L34GeF
```

Datenbank Konfiguration Datei **"my.ini"** in Datenbank Daten Verzeichnis anpassen *(D:\Apps\MariaDb\data\my.ini)*
anpassen (Nur die Kategorie ```[mysqld]```)

```ini
[mysqld]
datadir=D:/Apps/MariaDb/data
default_password_lifetime=0
bind-address=127.0.0.1
general_log=on
general_log_file=D:/Apps/MariaDb/query.log
```

Eintrag ```default_password_lifetime=0``` sorgt dafür das Password nie abläuft

```bind-address=127.0.0.1``` Eintrag sorgt dafür das der Datenbank nur auf localhost gebunden wird statt auf allen Schnittstellen

Die beiden weitere Einträge schalten Query log an und definieren die Logdatei


##### 2. Start/Stop Batch Dateien erstellen

```db-start.bat``` Löscht Datenbank Logdatei und starten Datenbank Server

```bat
@echo off


D:
cd D:\Apps\MariaDb

REM Alte Logdatei entfernen
del query.log

REM MariaDB Server starten
bin\mariadb.exe --datadir=D:/Apps/MariaDb/data --console
```

```db-stop.bat``` Stopt MariaDB Server

```bat
@echo off

REM Stop MariaDb Server
D:
cd D:\Apps\MariaDb\bin
mariadb-admin.exe -uroot -pjM10L34GeF shutdown
```


