### Idea IDE


#### Wichtigste Fenster 

|Beschreibung                         |Gui                                      |Tastenkombination              |
|:--                                  |:--                                      |:--                            |
|Projekt (Datein, Packages, etc.)     |View -> Toolwindows -> Project           |```Alt+1```                    |
|Struktur Fenster (Eclipse Outline)   |View -> Toolwindows -> Structure         |```Alt+7``` ```Ctr+F12```      |
|Suchergebnisse                       |View -> Toolwindows -> Find              |```Alt+3```                    |
|Lesezeichen                          |View -> Toolwindows -> Bookmarks         |                               |
|Javadoc                              |View -> Toolwindows -> Documentation     |```Ctr+q``` (Kontext basiert)  |

Mit Javadoc Fenster gibt es ab und an Probleme (Es wird nicht immer unter View -> Toolwinodws -> Documentation
angezeigt) dann einfach auf eine JDK Methode gehen und ```Ctr+q``` drücken.

#### Von mir hinzugefügte Tastenkombinationen

Als alle erstes die Tastenkombinationen unbinden ```Ctr+1, Ctr*2, Ctr+3, ect``` die werden für Bookmarks Navigation verwendet.

|Neue Tastenkombination     |Idea Tastenkombination     |Idea Action                                                        |
|:--                        |:--                        |:--                                                                |
|Ctr+1, T                   |                           |Main Menu > Code > Instert Live Template....                       |
|Ctr+1, S                   |Ctr+Alt+T                  |Main Menu > Code > Surround With...                                |
|Ctr+1, F                   |Ctr+Alt+Shift+L            |Main Menu > Code > Code Formatting Actions > Reformat File..       |
|Ctr+1, R                   |Shift+F6                   |Main Menu > Refactor > Rename...                                   |
|Ctr+1, M                   |F6                         |Main Menu > Refactor > Move...                                     |
|Ctr+2, L                   |Ctr+Alt+V                  |Main Menu > Refactor > Extract/Introduce > Introduce Variable...   |
|Ctr+2, F                   |Ctr+Alt+F                  |Main Menu > Refactor > Extract/Introduce > Introduce Field...      |
|Ctr+3                      |Ctr+Shift+3                |Main Menu > Help > Find Action...                                  |
|Ctr+4, I                   |                           |Main Menu > Code > Inspect Code Actions > Inspect Code...          |
|Ctr+4, A                   |Ctr+Shift+Alt+l            |Main Menu > Code > Inspect Code Actions > Analyze Code...          |
|Ctr+4, S                   |                           |Plugins > SonarLint > Analyze All Files with SonarLint             |

Tastenkombination mit Komma besteht aus zwei Komponenten. Erstes wird am Anfang stehende Tastenkombination gedrückt, 
dann folgt "Second storke:" Buchstabe. Für Tastenkombination ```Ctr+2,L``` drückt man erst einmal ```Ctr+2``` und dann 
drückt man Buchstabe ```L``` (Ohne Umschalten also Kleinbuchstabe).

#### Wichtigste Tastenkombinationen 

- Suche ```Shift+Shift``` - auswählen, was man suchen möchte -> All | Classes | Fieles | Symbols | Actions | Git | Text
- Eclipse quick Access ```Ctr+Shift+A``` man landet bei Suche -> Actions (ich belege es mit ```Ctr+3```)
- ```Ctr+MouseLeft```  zu Deklaration springen
- ```Ctr+Alt+j``` - erzeuge Schleife basierend auf Kontext  (Achtung kein ; sonst wird Typ nicht erkannt)
- ```Ctr+Alt+l``` code Formatierung  ```Ctr+Shift+.``` code Formatierung mit Einstellungen (Optimize imports, etc)
- ```Ctr+Alt+t``` Element mit z.B. try/catch if/else auflisten ```Ctr+Shift+Ent``` was umflossen wurde rückgängig machen
- ```Ctr+q``` Javadoc Kontextmenü ```Ctr+p``` Methoden Signatur anzeigen ```Ctr+Shif+i``` Methoden Implementation anzeigen
- ```Ctr+Shift+f``` In Dateien suchen (spezialisierte Maske) ```Ctr+Shift+r``` in Dateien suchen und ersetzen
- ```Ctr+Shift+e``` zuletzt angesehene Dateien

#### Debug

- ```Ctr+f``` ?
- ```Ctr+F9``` Hotswap reload changes + resetFrame

#### Markierungen 

- ```Ctr+Shift+F7``` Symbol unter dem Cursor hervorheben
- ```Ctr+Alt+Shift+j``` Alle Symbole in Dokument selektieren
- ```F2``` Nächstes Problem (Warnung / Fehler) Alternative (```Alt+Enter``` / ```Ctr+F2```)

#### Wichtigste Tastenkombinationen Nummernblock

- ```Ctr+Numblock /``` Zeile auskommentieren ```Ctr+Shift+Numblock /``` Zeilen Kommentar entfernen
- ```Ctr+Shift+Numblock(/)``` Auskommentieren Block ```Ctr+Shift+Numblock(/)``` Zeilen Kommentar entfernen
- ```Ctr+Numblock +``` falten ```Ctr+Numblock -``` entfalten
- ```Ctr+Shift+Numblock +``` alle Blöcke in Dokument falten ```Ctr+Shfit+Numblock -``` alle Blöcke in Dokument entfalten

#### Refaktoring

- ```Ctr+Alt+Shift+t``` Refaktoring menu (Kontext basiert)

### Konfiguration

- Editor > CodeStyle > Java > Imports > Use single class import 
- Editor > CodeStyle > Java > Class count to use import with '*'          100
- Editor > CodeStyle > Java > Class count to use static import with '*'   100
- Editor > Inlay Hints > Code vision > Usages
- Editor > General > Code Folding > File headers, Imports, etc
- Editor > General > Code Folding > One-line methods, etc
- Auf 3 vertikale Punkte neben Run / Debug Buttons drücken und dann "Add to Main Toolbar..." oder "Customize Main Toolbar..." um Copy/Paste Undo/Redo Navigation Forward / Back hinzfügen

### Inspection Profile Setting

- Reassigned variable (disable)

#### Wichtigste Live Templates Shortcuts

|Template   | Aktion Java / Pseudocode  |
|:--        |:--                        |
|```iter``` | ```foreach```             |
|```lazy``` | ```if null lazy init```   |
|```psf```  |```public static final```  |
|```main``` |```main method```          |

|Template   | Sysout / Pseudocode               |
|:--        |:--                                |
|```sout``` |```sysot.println```                |
|```soutm```|```sysout.println method-info```   |
|```soutv```|```sysout.println var = value```   |

##### Todo / Fixme Tempaltes

Es kam mir vor, als ob die Life Templates für ```todo``` und ```fixme``` in IDE bereits definiert wurden. Aber dem ist nicht so 
man muss diese Templates wohl selbst definieren. Wie man es macht wird hier erklärt: 
[IntelliJ Life Templates](https://www.jetbrains.com/help/idea/creating-and-editing-live-templates.html).
Ich habe folgenden Pattern verwendet (Beispiel TODO):

```
$TPL_START$ TODO $DATE$: $END$ $TPL_END$
```

**Pattern Variables:**

|Variable   |Expression     |Default value  |Skip if defined    |
|:--        |:--            |:--            |:--                |
|TPL_START  |commentStart() |None           |Checked            |
|DATE       |date()         |None           |Checked            |
|TPL_END    |commentEnd()   |None           |Checked            |

*Appplicable Context - alles auswählen*

|Context_ Template  | Java / Pseudocode             | Tasten Kombination
|:--                |:--                            |:--
|```_.iter```       | ```foreach x in _```          |```Ctr+Alt+j```
|```_.twr```        | ```try resource = _```        |
|```_.sotv```       |```sysout.println _ = value``` |
|```_.lazy```       |```if null lazy init _```      |
|```_.null```       |```if _ null```                |
|```_.nn```         |```if _ not null```            |
|```_.field```      |```_.return to field```        |```Ctr+Alt+f```
|```_.var```        |```_.return to var```          |```Ctr+Alt+v```
|```_.lambda```     |```to lambda```                |


#### Fragwürdige Funktionen

- ```Alt+J``` Symbol unter dem Cursor selektieren, ```Alt+Shift+J``` Selektion aufheben, danach selektierte einfach verändern
- Tippe ```for``` dann ```Ctr+Shift+Enter``` tippe ```if```i dann ```Ctr+Shift+Enter``
- ```Ctr+Space``` dann ```Tab```
-  ```Ctr+w``` Markierung starten / erweitern ```Ctr+Shift+w``` Markierung verlieren / aufheben (Geht nicht mit Vi Plugin)

