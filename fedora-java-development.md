#### Java Entwicklungs Umgebung Konfiguration auf Fedora System

#### Pfade

| Subjekt		| Pfad 			| Notiz 				                        |
|-----------------------|-----------------------|---------------------------------------------------------------|
| Root			| $HOME/jd		| Java Entwicklugs Umgebung (jd) Root. Umgebungs Variable $JD	|
| Eclipse		| $JEU/Eclipse		| Eclipse Installation Pfad					|
| JDK			| (Fedora)		| JDK wird Fedora verwaltet					|
| Maven			| (Fedora)		| Maven wird von Fedora verwaltet				|
| Starter Script	| $JD/Scripts/init	| Initialisiert Java Entwicklungs Umgebung			|


#### Einrichtung init script

Es wird angenohmen dieses Projekt (java-notizen) ist in lokalen Dateisystem enthalten. Von `init` Script wird
ein hartes link in `jd/Scripts` Verzeichniss erzuegt.

```sh
cd ~
mkdir -p jd/Scripts
cd jd/Scripts
ln $home/java-notizen/init init
```

Dannach .bashrc um folgende Zeilen ergenzen

```bash
# Java Entwicklunsg Umgebung konfigurieren
JD_INIT=$(realpath ~/jd/Scripts/init)
if [ -f "$JD_INIT" ]
then
    source "$JD_INIT"
fi
```

Prüfen ob .bashrc ohne "Probleme" geladen werden kann

```sh
source .bashrc
```

