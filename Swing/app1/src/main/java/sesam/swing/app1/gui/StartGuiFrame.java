package sesam.swing.app1.gui;

import javax.swing.JFrame;

public class StartGuiFrame implements Runnable {
	
	private JFrame frame;
	
	@Override
	public void run() {
		frame = new JFrame();
		frame.setTitle("App-1");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setSize(1024, 1024);
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}

		

}
