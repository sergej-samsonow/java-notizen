package sesam.swing.app1;

import javax.swing.SwingUtilities;

import sesam.swing.app1.gui.StartGuiFrame;

public class App1 {
	

	void startGui() {
		SwingUtilities.invokeLater(new StartGuiFrame());
	}

	public static void main(String[] args) {
		var app = new App1();
		app.startGui();
	}

}
